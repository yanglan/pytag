# pytag.py
# Author(s): Yang Lan <yanglan.physics@gmail.com> 20180127
# A python script for sorting/categorizing documents by creating shortcuts based on tags in filename
# tags are enclosed by a pari of square brackets [] located at anywhere of the filename
# two groups of tags: A_tags and B_tags
# A_tags: year author type collaboration
# A_tags can be skipped but must be in order (skip collaboration or skip type and collaboration et. al.)
# B_tags: fields
# A_tags and B_tags separated by "; " (;+space)
# tags in each group separated by one space
# each tag can have parallels item using +
# each tag can have sub_tags using , or .
# example file names:
# [2013 Li paper.prl mcgill+argonne; ion,trapping]Tensor Interaction Limit Derived From the _PhysRevLett.110.092502
# [2010 Hagen book; electronics rf]Radio-FrequencyElectronics_JonHagen_2010.pdf


import os
import sys
import pdb
import time
import shutil


from win32com.client import Dispatch


def createShortcut(path, target='', wDir='', icon=''):
    """ Taken from: https://www.blog.pythonlibrary.org/2010/01/23/using-python-to-create-shortcuts/"""
    ext = path[-3:]
    if ext == 'url':
        shortcut = file(path, 'w')
        shortcut.write('[InternetShortcut]\n')
        shortcut.write('URL=%s' % target)
        shortcut.close()
    else:
        shell = Dispatch('WScript.Shell')
        shortcut = shell.CreateShortCut(path)
        shortcut.Targetpath = target
        shortcut.WorkingDirectory = wDir
        if icon == '':
            pass
        else:
            shortcut.IconLocation = icon
        shortcut.save()


def shortcutTree(dirname, fn):
    """ either . or , results subdirectories"""
    dirname = dirname.replace(',', os.sep)
    dirname = dirname.replace('.', os.sep)
    # use lower cases for all directories 20180222_173749
    dirname = dirname.lower()
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    dirname_tree = dirname.split(os.sep)
    dirname_use = ''
    for dirname_current in dirname_tree:
        dirname_use += dirname_current + os.sep
        createShortcut(dirname_use + fn + '.lnk', cwd + fn)


def parallelTags(pre_tag, paratags, fn):
    parallel_tags = paratags.split('+')
    for parallel_tag in parallel_tags:
        tag = pre_tag + os.sep + parallel_tag
        shortcutTree(tag, fn)


def typeTags(pre_tag, paratags, fn):
    """Add sub tags into each document type directory. e.g. papers, book"""
    type_tags = A_tags[2].split('+')
    for type_tag in type_tags:
        full_pre_tag = A_tag_names[2] + os.sep + \
            type_tag + os.sep + pre_tag + os.sep
        parallelTags(full_pre_tag, paratags, fn)
        # skip the journal/publisher
        type_tag = type_tag.split(',')[0]
        type_tag = type_tag.split('.')[0]
        # short_pre_tag skips the journal/publisher name
        short_pre_tag = A_tag_names[2] + os.sep + \
            type_tag + os.sep + pre_tag + os.sep
        parallelTags(short_pre_tag, paratags, fn)


A_tag_names = ['year', 'author', 'type', 'collaboration']

fn_full = sys.argv[1]
# filename
fn = os.path.basename(fn_full)
cwd = os.path.dirname(fn_full) + os.sep

# make an original copy
original_dir = 'G:\doc\originals'
if not os.path.exists(original_dir):
    os.makedirs(original_dir)  # , exist_ok=True)
# 20190706_125516 put the originals under a directory of their own category
caterory_dir = fn_full.split(os.sep)[-2]
original_dir = original_dir + os.sep + caterory_dir
original_fn = original_dir + os.sep + fn
if not os.path.exists(original_dir):
    os.mkdir(original_dir)
if not os.path.exists(original_fn):
    shutil.copy2(fn, original_fn)


# fn = '[2007 Lunney paper titan; ionoptics rfq lpt].pdf'
# get tags inside [] from filename
fn_tags = fn[fn.find('[') + 1:fn.find(']')]
A_tags, B_tags = fn_tags.split('; ')
A_tags = A_tags.split(' ')
B_tags = B_tags.split(' ')


# iterate through the A_tags
for i, A_tag in enumerate(A_tags):
    A_tag_name = A_tag_names[i]
    pre_tag = A_tag_name
    parallelTags(pre_tag, A_tag, fn)


# add sub tags for each collaboration directory, skip the A_tag_name inside each collaboration
if len(A_tags) > 3:
    for i, A_tag in enumerate(A_tags):
        if i == 3:
            continue
        collaboration_tags = A_tags[3].split('+')
        for collaboration_tag in collaboration_tags:
            pre_tag = A_tag_names[3] + os.sep + collaboration_tag
            parallelTags(pre_tag, A_tag, fn)

# add sub tags for each document type
for i, A_tag in enumerate(A_tags):
    if i == 2:
        continue
    pre_tag = A_tag_names[i]
    typeTags(pre_tag, A_tag, fn)


B_tag_name = 'field'
# iterate through B_Tags
for B_tag in B_tags:
    tag = B_tag_name + os.sep + B_tag
    shortcutTree(tag, fn)
    pre_tag = B_tag_name
    typeTags(pre_tag, B_tag, fn)


print(fn + ' added to shortcuts')
# time.sleep(1)
# pdb.set_trace()
# input('press any key to exit')
