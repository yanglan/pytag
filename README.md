A python script for sorting/categorizing documents by creating shortcuts based on tags in filename.
	Tags are enclosed by a pari of square brackets [] located at anywhere of the filename.

Two groups of tags: A_tags and B_tags.
	A_tags: year author type collaboration.
		A_tags can be skipped but must be in order (skip collaboration or skip type and collaboration et. al.).
	B_tags: fields

	A_tags and B_tags separated by "; " (;+space).
	Tags in each group separated by one space
	Each tag can have parallels item using +
	each tag can have sub_tags using . (dot)

example file names:
[2013 Li paper.prl mcgill+argonne; ion,trapping]Tensor Interaction Limit Derived From the _PhysRevLett.110.092502

[2010 Hagen book; electronics rf]Radio-FrequencyElectronics_JonHagen_2010.pdf

Currently only tested in Windows 10. The function createShortcut in tag.py needed to be adapted for it to work in Mac/Linux.

Two ways to run the script:
	1. Place tag.py and tag_all.py in the same directory with the documents. Run tag_all.py
	2. Add tag.py to the right-click context menu so that it works for individual documents (maximum 15 documents each time, otherwise the context menu doesn't work).
		How to add context menu: https://www.howtogeek.com/howto/windows-vista/add-any-application-to-the-desktop-right-click-menu-in-vista/
		Value data in my registry: C:\Anaconda2\python.exe C:\Users\yang\OneDrive\python_packages\pytag\tag.py "%1"