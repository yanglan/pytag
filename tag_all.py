import glob
import os
import pdb
pdfnames = glob.glob('*.pdf')
cwd = os.getcwd() + os.sep
for pdfname in pdfnames:
    pdfname = cwd + pdfname
    cmd = 'python tag.py "%s"' % pdfname
    os.system(cmd)
